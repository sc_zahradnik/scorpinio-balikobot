<?php
namespace Scorpinio\Balikobot;

class Request{
	///////////////
	// CONSTANTS //
	///////////////
	const METHOD_CONNECT = "CONNECT";
	const METHOD_OPTIONS = "OPTIONS";
	const METHOD_DELETE = "DELETE";
	const METHOD_PATCH = "PATCH";
	const METHOD_TRACE = "TRACE";
	const METHOD_HEAD = "HEAD";
	const METHOD_POST = "POST";
	const METHOD_PUT = "PUT";
	const METHOD_GET = "GET";

	////////////////////////
	// PRIVATE PROPERTIES //
	////////////////////////
	private $headers;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * @param  array $headers request headers
	 * @return void
	 */
	public function __construct($headers=null){
		if (!is_null($headers)) {
			$this->headers = $headers;
		}
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function get($url, $data = array(), $options = array()){
		$url = $url.((!empty($data))?"?".http_build_query($data):"");
		$curl = new curl(array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
		));

		$this->_curlSetOptions($curl, $options);
		$this->_curlSetHeaders($curl);

		//execute request
		$response = $curl->exec();

		// get response headers
		$this->headers = $curl->getInfo();

		$response = $this->decodeResponse($curl, $response);

		$curl->close();

		return $response;
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function post($url, $data, $options = array()){
		if (array_search("Content-Type: application/json",$this->headers) !== false 
			&& json_decode($data) == false) {
			$data = json_encode($data);
		}

		$curl = new curl(array(
			CURLOPT_URL => $url,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => (string)$data,
			CURLOPT_RETURNTRANSFER => true,
		));

		$this->_curlSetOptions($curl, $options);
		$this->_curlSetHeaders($curl);

		// execute request
		$response = $curl->exec();

		// get response headers
		$this->headers = $curl->getInfo();

		$response = $this->decodeResponse($curl, $response);

		$curl->close();

		return $response;
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function put($url, $data, $options = array()){
		return $this->customeMethod(self::METHOD_PUT, $url, $data, $options);
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function patch($url, $data, $options = array()){
		return $this->customeMethod(self::METHOD_PATCH, $url, $data, $options);
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function head($url, $data, $options = array()){
		return $this->customeMethod(self::METHOD_HEAD, $url, $data, $options);
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function trace($url, $data, $options = array()){
		return $this->customeMethod(self::METHOD_TRACE, $url, $data, $options);
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function connect($url, $data, $options = array()){
		return $this->customeMethod(self::METHOD_CONNECT, $url, $data, $options);
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function options($url, $data, $options = array()){
		return $this->customeMethod(self::METHOD_OPTIONS, $url, $data, $options);
	}
	/**
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	public function delete($url, $data, $options = array()){
		return $this->customeMethod(self::METHOD_DELETE, $url, $data, $options);
	}
	/**
	 * @param array $headers
	 */
	public function setHerders($headers){
		$this->headers = $headers;
	}
	/**
	 * @param string $value header value
	 * @param mixed  $key   header name
	 */
	public function addHeader($value, $key=null){
		if (is_null($key)) {
			$this->headers[] = $value;
		}else{
			$this->headers[$key] = $value;
		}
	}
	/**
	 * @return array curent http headers
	 */
	public function getHttpHeaders(){
		return $this->headers;
	}

	//////////////////////
	// STATIC FUNCTIONS //
	//////////////////////

	/**
	 * @return array class constants - available http methods
	 */
	public static function getMethods(){
		$cls = new \ReflectionClass(__CLASS__);
		return $cls->getConstants();
	}

	///////////////////////
	// PRIVATE FUNCTIONS //
	///////////////////////

	/**
	 * @param  string $method  request http method
	 * @param  string $url     requested url
	 * @param  array  $data    request data
	 * @param  array  $options curl options
	 * @return mixed           response
	 */
	private function customeMethod($method, $url, $data, $options){
		$curl = new curl(array(
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_RETURNTRANSFER => true,
		));

		switch ($method) {
			case self::METHOD_PUT:
			case self::METHOD_PATCH:
				$curl->setopt(CURLOPT_POSTFIELDS, (string)$data);
			break;
			case self::METHOD_DELETE:
			default:
				$curl->setopt(CURLOPT_URL, $url."?".http_build_query($data));
			break;
		}

		$this->_curlSetOptions($curl, $options);
		$this->_curlSetHeaders($curl);

		// execute request
		$response = $curl->exec();

		// get response headers
		$this->headers = $curl->getInfo();

		$response = $this->decodeResponse($curl, $response);

		$curl->close();

		return $response;
	}
	/**
	 * @param  curl  $curl     instance of curl
	 * @param  mixed $response response
	 * @return mixed           decoded response
	 */
	private function decodeResponse(curl $curl, $response){
		if (strpos($curl->getInfo(CURLINFO_CONTENT_TYPE), "application/json") !== false) {
			$response = json_decode($response, true);
			$code = json_last_error();
			if($code !== JSON_ERROR_NONE){
				$msg = "Balikobot API: JSON ERROR (".$code.") ";
				throw new \Exception($msg, $code);
			}
		}
		return $response;
	}
	/**
	 * @param  array $options curl options
	 * @return array 
	 */
	private function clearOpt($options){
		$disallow = array(
			CURLOPT_URL,
			CURLOPT_POST,
			CURLOPT_RETURNTRANSFER,
			CURLOPT_HTTPHEADER,
		);
		foreach ($options as $key => $value) {
			if (in_array($key, $disallow)) {
				unset($options[$key]);
			}
		}
		return $options;
	}
	/**
	 * @param  curl   $curl    instance of curl
	 * @param  array  $options curl options
	 * @return void
	 */
	private function _curlSetOptions(curl $curl, $options){
		if (!empty($options)) {
			$options = $this->clearOpt($options);
			if (!empty($options)) {
				$curl->setoptArray($options);
			}
		}
	}
	/**
	 * @param  curl   $curl instance of curl
	 * @return void
	 */
	private function _curlSetHeaders(curl $curl){
		if (!empty($this->headers)) {
			$curl->setopt(CURLOPT_HTTPHEADER, $this->headers);
		}
	}
}