<?php
namespace Scorpinio\Balikobot\abstracts;

use Scorpinio\Balikobot\interfaces\Entity as IEntity;
use Scorpinio\Balikobot\Request;

abstract class Entity implements IEntity{

	/////////////////////
	// PUBLIC FUNCTION //
	/////////////////////

	/**
	 * [isValid description]
	 * @return boolean [description]
	 */
	public function isValid(){
		if (in_array(self::METHOD, array(Request::METHOD_GET,Request::METHOD_PUT,Request::METHOD_PATCH,Request::METHOD_POST,Request::METHOD_DELETE))) {
			if (!empty($this->url)) {
				return true;
			}else{
				//throw
			}
		}else{
			//throw
		}
	}
	/**
	 * Prepare data for current request
	 * @return mixed
	 */
	public function getData(){
		$data = $this->getProperties(true);
		foreach ($data as $property => $value) {
			if (is_null($value)) {
				unset($data[$property]);
			}
			if (is_array($value) && count($value) > 0) {
				$data[$property] = $this->prepareSubData($value);
			}elseif ($value instanceof Entity) {
				$data[$property] = $value->getData();
			}
		}
		return $data;
	}
	/**
	 * @param  array $array 
	 * @return array
	 */
	public function prepareSubData($array){
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$array[$key] = $this->prepareSubData($value);
			}
			if ($value instanceof Entity) {
				$array[$key] = $value->getData();
			}
		}
		return $array;
	}
	/**
	 * Fill entity properties. Array key is property name, array value is property value
	 * @param  array  $data
	 * @return void
	 */
	public function fillData($data){
		if (is_array($data)) {
			$keys = array_keys($data);
			if (is_string($keys[0])) {
				foreach ($data as $property => $value) {
					$method = 'set'.ucfirst($property);
					$this->$method($value);
				}
			}elseif (is_numeric($keys[0])) {
				foreach ($this->getProperties() as $index => $property) {
					if (array_key_exists($data, $index)) {
						$method = 'set'.ucfirst($property);
						$this->$method($data[$index]);
					}
				}
			}else{
				//thow
			}
		}
	}
	/**
	 * @param  boolean $values 
	 * true = return array of properties with values 
	 *        where array key is property name
	 *        and array value is property value
	 * false = return array of properties 
	 *        where array value is property name
	 * @return array
	 */
	public function getProperties($values = false){
		if ($values === true) {
			return get_object_vars($this);
		}else{
			return array_keys(get_object_vars($this));
		}
	}
	/**
	 * @param array $data initial fill properties 
	 */
	public function __construct($data = null){
		if (!is_null($data)) {
			$this->fillData($data);
		}
	}
	/**
	 * @return string json encoded properties
	 */
	public function __toString(){
		return json_encode(get_object_vars($this));
	}
	/**
	 * @return array entity properties in array
	 */
	public function toArray(){
		return $this->getProperties(true);
	}
	/**
	 * @param  string $method    called entity method
	 * @param  array $arguments arguments of called entity methods
	 * @return void
	 */
	public function __call($method, $arguments){
		if(preg_match("/^([sg]{1}et|add)(.*)/", $method, $method)){
			if (is_array($method)) {
				$property = lcfirst($method[2]);
				$method = strtolower($method[1]);
				if ($arguments) {
					$value = $arguments[0];
				}
			}else{
				$property = strtolower($arguments[0]);
				$value = $arguments[1];
			}

			$properties = $this->getProperties();

			if (in_array(lcfirst($property), $properties) || in_array(ucfirst($property), $properties)) {
				if (in_array(lcfirst($property), $properties)) {
					$property = lcfirst($property);
				}elseif(in_array(ucfirst($property), $properties)){
					$property = ucfirst($property);
				}
				switch ($method) {
					case 'set':
						$this->$property = $value;
						return $this;
					break;
					case 'add':
						if (is_array($this->$property)) {
							$array = $this->$property;
							$array[] = $value;
							$this->$property = $array;
							return $this;
						}
					break;
					case 'get':
						return $this->$property;
					break;
				}
			}else{
				throw new \Exception("Property (".get_class($this)."::$".$property.") does not exist.", 1);
			}
		}
	}
}