<?php
namespace Scorpinio\Balikobot;

use Scorpinio\Balikobot\abstracts\Entity;

class Api{
	///////////////
	// CONSTANTS //
	///////////////
	const API_HOST = "https://api.balikobot.cz/";

	////////////////////////
	// PRIVATE PROPERTIES //
	////////////////////////
	private $user;
	private $key;


	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * 
	 * @param string $user Balikobot login
	 * @param string $key  Balikobot password
	 * @return void
	 */
	public function __construct($user, $key){
		$this->user = $user;
		$this->key = $key;
	}

	/**
	 * 
	 * @param  Entity        $requestData Request Entity
	 * @return mixed                      Response from Balikobot
	 */
	public function request(Entity $requestData){
		// prepare request data
		$data = $requestData->getData();

		// init request
		$request = new Request(array(
			"Content-Type: application/json",
			"Authorization: Basic ".base64_encode($this->user.":".$this->key),
		));

		// request url
		$url = self::API_HOST.$requestData->getUrl();

		// request options
		$options = array(
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
		);

		switch ($requestData::METHOD) {
			case Request::METHOD_POST:
				$response = $request->post($url, $data, $options);
			break;
			case Request::METHOD_PUT:
				$response = $request->put($url, $data, $options);
			break;
			case Request::METHOD_PATCH:
				$response = $request->patch($url, $data, $options);
			break;
			case Request::METHOD_DELETE:
				$response = $request->delete($url, $data, $options);
			break;
			case Request::METHOD_GET:
			default:
				$response = $request->get($url, $data, $options);
			break;
		}

		return $response;
	}
}