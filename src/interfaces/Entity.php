<?php
namespace Scorpinio\Balikobot\interfaces;

interface Entity{
	/**
	 * 
	 * @return boolean
	 */
	public function isValid();
	/**
	 * Prepare data for current request
	 * @return mixed
	 */
	public function getData();
}