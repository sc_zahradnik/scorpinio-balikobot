<?php
namespace Scorpinio\Balikobot;

/**
* curl functions wrapper
*/
class curl{
	////////////////////////
	// PRIVATE PROPERTIES //
	////////////////////////
	private $handler;
	
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * @param array $options curl options
	 */
	public function __construct($options = array()){
		$this->init();
		if (count($options) > 0) {
			$this->setoptArray($options);
		}
	}
	/**
	 * @return void
	 */
	public function init(){
		$this->handler = curl_init();
	}
	/**
	 * @param  array $options curl options
	 * @return bool
	 */
	public function setoptArray($options){
		return curl_setopt_array($this->handler, $options);
	}
	/**
	 * @param  int   $option curl option
	 * @param  mixed $value  value for curl option
	 * @return bool
	 */
	public function setopt($option, $value){
		return curl_setopt($this->handler, $option, $value);
	}
	/**
	 * @return mixed
	 */
	public function exec(){
		return curl_exec($this->handler);
	}
	/**
	 * @return void
	 */
	public function close(){
		curl_close($this->handler);
	}
	/**
	 * @return int error number
	 */
	public function errno(){
		return curl_errno($this->handler);
	}
	/**
	 * @return string error message
	 */
	public function error(){
		return curl_error($this->handler);
	}
	/**
	 * @param  string $string string to be encoded
	 * @return string         encoded string
	 */
	public function escape($string){
		return curl_escape($this->handler, $string);
		
	}
	/**
	 * @param  int $option curl option
	 * @return mixed
	 */
	public function getInfo($option = null){
		if (is_null($option)) {
			return curl_getinfo($this->handler);
		}
		return curl_getinfo($this->handler, $option);
	}
	/**
	 * @return void
	 */
	public function reset(){
		curl_reset($this->handler);
	}
	/**
	 * @param  string $string URL encoded string to be decoded
	 * @return string         decoded string
	 */
	public function unescape($string){
		return curl_unescape($this->handler, $string);
	}
	/**
	 * @param  int $age curl version
	 * @return array
	 */
	public function version($age = CURLVERSION_NOW){
		return curl_version($this->handler, $age);
	}
}