<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Services extends Entity{
	const METHOD = Request::METHOD_POST;

	protected $url;
	protected $carrier;

	public function __construct(Carrier $carrier){
		$url = $carrier::ID."/services";

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
		));
	}

	public function getData(){
		return null;
	}
}	