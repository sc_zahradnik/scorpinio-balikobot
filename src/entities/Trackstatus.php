<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Trackstatus extends Entity{
	const METHOD = Request::METHOD_POST;

	protected $url;
	protected $carrier;
	protected $packages;

	public function __construct(Carrier $carrier, $packages = array()){
		if ($packages instanceof Package) {
			$packages = array($packages);
		}elseif(is_array($packages)){
			foreach ($packages as $key => $package) {
				if ($package instanceof Package) {
					
				}
			}
		}else{
			throw new \Exception("Scorpinio\\Balikobot\\entities\\Add::_construct - $packages must by instanceof Scorpinio\Balikobot\entities\Package or array of it.");
		}

		$url = $carrier::ID."/trackstatus";

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
			'packages' => $packages,
		));
	}

	public function getData(){
		$data = array();
		foreach ($this->packages as $key => $package) {
			$data[] = array('id' => $package->getId_carrier());
		}
		return $data;
	}
}