<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Branches extends Entity{
	///////////////
	// CONSTANTS //
	///////////////
	const METHOD = Request::METHOD_POST;

	//////////////////////////
	// PROTECTED PROPERTIES //
	//////////////////////////

	protected $url;
	protected $carrier;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * [__construct description]
	 * @param Carrier     $carrier     [description]
	 * @param string|int  $serviceType [description]
	 */
	public function __construct(Carrier $carrier, $serviceType){
		$url = $carrier::ID."/branches/".$serviceType;

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
		));
	}
	/**
	 * [getData description]
	 * @return [type] [description]
	 */
	public function getData(){
		return null;
	}
}