<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;

class Carrier extends Entity{
	///////////////////////
	// PUBLIC PROPERTIES //
	///////////////////////
	public $serviceType;

	//////////////////////////
	// PROTECTED PROPERTIES //
	//////////////////////////

	protected $rf;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * [__construct description]
	 * @param [type] $serviceType [description]
	 */
	public function __construct($serviceType){
		parent::__construct(array(
			'serviceType' => $serviceType,
		));
		$this->rf = new \ReflectionClass($this);
	}
	/**
	 * [__toString description]
	 * @return string [description]
	 */
	public function __toString(){
		$id = $this->rf->getConstant("ID");
		return $id."|".$this->serviceType;
	}
	/**
	 * [getName description]
	 * @param  boolean $incServiceType [description]
	 * @return [type]                  [description]
	 */
	public function getName($incServiceType=false){
		if ($incServiceType === true) {
			return $this->rf->getConstant("NAME")." ".$this->services[$this->serviceType];
		}
		return $this->rf->getConstant("NAME");
	}
	/**
	 * [getId description]
	 * @param  boolean $incServiceType [description]
	 * @return [type]                  [description]
	 */
	public function getId($incServiceType=false){
		if ($incServiceType === true && !empty($this->serviceType)) {
			return $this->rf->getConstant("ID")."|".$this->serviceType;
		}
		return $this->rf->getConstant("ID");
	}
	/**
	 * [getNameWithServices description]
	 * @return [type] [description]
	 */
	public function getNameWithServices(){
		$names = array();
		foreach ($this->services as $serviceTypeId => $serviceType) {
			$this->serviceType = $serviceTypeId;
			$names[$this->getId()."|".$serviceTypeId] = $this->getName(true);
		}
		$this->serviceType = null;
		return $names;
	}
	/**
	 * [getCarrierTrackUrl description]
	 * @param  string|int $id       [description]
	 * @param  string     $name     [description]
	 * @param  string     $lastname [description]
	 * @return [type]               [description]
	 */
	public function getCarrierTrackUrl($id, $name="", $lastname=""){
		$url = $this->rf->getConstant("TRACK_URL");
		$url = preg_replace("/\{package_id\}/", $id, $url);
		$url = preg_replace("/\{name\}/", $id, $url);
		$url = preg_replace("/\{lastname\}/", $id, $url);
		return $url;
	}

}