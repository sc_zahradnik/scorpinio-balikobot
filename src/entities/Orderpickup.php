<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Orderpickup extends Entity{
	///////////////
	// CONSTANTS //
	///////////////
	const METHOD = Request::METHOD_POST;

	//////////////////////////
	// PROTECTED PROPERTIES //
	//////////////////////////

	protected $url;
	protected $carrier;
	protected $date;
	protected $time_from;
	protected $time_to;
	protected $weight;
	protected $package_count;
	protected $message;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * [__construct description]
	 * @param Carrier $carrier      [description]
	 * @param string  $date         [description]
	 * @param string  $timeFrom     [description]
	 * @param string  $timeTo       [description]
	 * @param [type]  $weight       [description]
	 * @param int     $packageCount [description]
	 * @param string  $message      [description]
	 */
	public function __construct(Carrier $carrier, string $date, string $timeFrom, string $timeTo, $weight, int $packageCount, string $message = ""){

		$url = $carrier::ID."/orderpickup";

		parent::__construct(array(
			'url'           => $url,
			'carrier'       => $carrier,
			'date'          => $date,
			'time_from'     => $timeFrom,
			'time_to'       => $timeTo,
			'weight'        => $weight,
			'package_count' => $packageCount,
			'message'       => $message,
		));
	}
	/**
	 * [getData description]
	 * @return [type] [description]
	 */
	public function getData(){
		$data = parent::getData();
		$data['date'] = date("Y-m-d", strtotime($data['date']));
		unset($data['url']);
		unset($data['carrier']);
		return $data;
	}
}