<?php
namespace Scorpinio\Sdelit\RestApi\entities;

use Scorpinio\Sdelit\RestApi\abstracts\Entity;
use Scorpinio\Sdelit\RestApi\Request as MRequest;

class Request extends Entity{
	protected $method;
	protected $url;
	protected $data;

	public function __construct($method, $url, Entity $data = null){
		if (!in_array($method, MRequest::getMethods())) {
			throw new Exception("Method Not Implemented", 501);
		}
		parent::__construct([
			'method' => $method,
			'url'    => $url,
			'data'   => $data,
		]);
	}
}