<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Labels extends Entity{
	///////////////
	// CONSTANTS //
	///////////////
	const METHOD = Request::METHOD_POST;

	//////////////////////////
	// PROTECTED PROPERTIES //
	//////////////////////////

	protected $url;
	protected $carrier;
	protected $packages;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * [__construct description]
	 * @param Carrier $carrier  [description]
	 * @param [type]  $packages [description]
	 */
	public function __construct(Carrier $carrier, $packages){
		$url = $carrier::ID."/labels";

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
			'packages' => $packages,
		));
	}
	/**
	 * [getData description]
	 * @return [type] [description]
	 */
	public function getData(){
		return array('package_ids' => $this->packages);
	}
}