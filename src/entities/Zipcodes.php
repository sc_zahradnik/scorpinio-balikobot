<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Zipcodes extends Entity{
	const METHOD = Request::METHOD_POST;

	protected $url;
	protected $carrier;

	public function __construct(Carrier $carrier, $serviceType, $country=""){
		$url = $carrier::ID."/zipcodes/".$serviceType;
		if (!empty($country)) {
			$url .= "/".$country;
		}

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
		));
	}

	public function getData(){
		return null;
	}
}