<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Fullbranches extends Entity{
	///////////////
	// CONSTANTS //
	///////////////
	const METHOD = Request::METHOD_POST;

	//////////////////////////
	// PROTECTED PROPERTIES //
	//////////////////////////

	protected $url;
	protected $carrier;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * [__construct description]
	 * @param Carrier $carrier     [description]
	 * @param [type]  $serviceType [description]
	 */
	public function __construct(Carrier $carrier, $serviceType){
		$url = $carrier::ID."/fullbranches/".$serviceType;

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
		));
	}
	/**
	 * [getData description]
	 * @return [type] [description]
	 */
	public function getData(){
		return null;
	}
}