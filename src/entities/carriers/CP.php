<?php
namespace Scorpinio\Balikobot\entities\carriers;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\entities\Carrier;

class CP extends Carrier{
	///////////////
	// CONSTANTS //
	///////////////
	const ID = "cp";
	const NAME = "Česká pošta";
	const TRACK_URL = "https://www.postaonline.cz/trackandtrace/-/zasilka/cislo?parcelNumbers={package_id}";

	///////////////////////
	// PUBLIC PROPERTIES //
	///////////////////////

	//
	public $services = array(
		"DR" => "balík Do ruky",
		"NP" => "balík Na poštu",
		"RR" => "doporučená zásilka (do 0.05 kg)",
		"DV" => "balik Do ruky pro vybrané podavatele",
		"VL" => "cenné psaní",
		"SR" => "RR bez udané váhy (auto 0.04 kg)",
		"BA" => "doporučený balíček",
		"BB" => "cenný balík",
		"BN" => "balík nadrozměr",
		"NB" => "balík do balíkovny",
	);
	//přenest do validatoru
	public $required_properties = array(
		'rec_name', 'rec_email', 'rec_zip', 'rec_country', 'price', 'eid', 'service_type',
	);

	/////////////////////
	// PUBLIC FUNCTION //
	/////////////////////

	/**
	 * [isValid description]
	 * @return boolean [description]
	 */
	public function isValid(){
		parent::isValid();

		$invalid = array();

		$data = $this->package->getData();
		/* Required */
		foreach (self::REQUIRED_PROPERTIES as $property) {
			if (!array_key_exists($property, $data)) {
				$invalid['missing'][] = $property;
			}
		}

		/* Required if... */
		if (isset($data['rec_phone']) && preg_match("/^+420\d{9}/", $data['rec_phone']) === false) {
			$invalid['badFormat']['rec_phone'] = $data['rec_phone'];
		}
		if (!isset($data['rec_street']) && $data['service_type'] !== "NP") {
			$invalid['missing'][] = "rec_street";
		}
		if (!isset($data['rec_city']) && $data['service_type'] !== "NP") {
			$invalid['missing'][] = "rec_city";
		}
		if (!isset($data['cod_currency']) && isset($data['cod_price'])) {
			$invalid['missing'][] = "cod_currency";
		}
		if (isset($data['cod_price']) && (!isset($data['vs']) || strlen($data['vs']) > 10)) {
			if (!isset($data['vs'])) {
				$invalid['missing'][] = "vs";
			}else{
				$invalid['badFormat']['vs'] = $data['vs'];
			}
		}

		return $invalid;
	}
}