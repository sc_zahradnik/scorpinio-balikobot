<?php
namespace Scorpinio\Balikobot\entities\carriers;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\entities\Carrier;

class Geis extends Carrier{
	///////////////
	// CONSTANTS //
	///////////////
	const ID = "geis";
	const NAME = "Geis";
	const TRACK_URL = "http://tt.geis.cz/TrackAndTrace/ZasilkaDetailCargo.aspx?id={package_id}";

	///////////////////////
	// PUBLIC PROPERTIES //
	///////////////////////

	//
	public $services = array(
		1 => "Soukromá zásilka (B2C)",
		2 => "Firemní zásilka",
		3 => "Mezinárodní zásilka",
		4 => "Cargo CZ vnitrostátní zásilka",//gais cargo
		5 => "Carrgo mezinárodní zásilka",//gais cargo
		6 => "Gais Point",
	);
	//přenest do validatoru
	public $required_properties = array(
		'rec_name', 'rec_email', 'rec_street', 'rec_city', 'rec_zip',
		'rec_country', 'service_type', 'eid',
	);
}