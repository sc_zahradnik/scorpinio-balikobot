<?php
namespace Scorpinio\Balikobot\entities\carriers;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\entities\Carrier;

//asi sloučit s klasickým ppl nebo není nasazeno a nelze použít
class PPLSprint extends Carrier{
	///////////////
	// CONSTANTS //
	///////////////
	const ID = "ppl";
	const NAME = "PPL Sprint";
	const TRACK_URL = "https://www.ppl.cz/main2.aspx?cls=Package&idSearch={package_id}";

	///////////////////////
	// PUBLIC PROPERTIES //
	///////////////////////

	//
	public $services = array(
		"2" => "PPL Parcel Connect (exportní balík)",
		"3" => "PPL Parcel CZ Dopolední balík",
		"4" => "PPL Parcel CZ Private (soukromý balík)",
		"8" => "PPL Parcel CZ Business (firemní balík)",
		"9" => "PPL Parcel CZ Private - večerní doručení",
	);
	//přenest do validatoru
	public $required_properties = array(
		'rec_name', 'rec_email', 'rec_street', 'rec_city', 'rec_zip',
		'rec_country', 'eid',
	);
}