<?php
namespace Scorpinio\Balikobot\entities\carriers;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\entities\Carrier;

class DPD extends Carrier{
	///////////////
	// CONSTANTS //
	///////////////
	const ID = "dpd";
	const NAME = "DPD";
	const TRACK_URL = "https://tracking.dpd.de/parcelstatus?query={package_id}&locale=cs_CZ";

	///////////////////////
	// PUBLIC PROPERTIES //
	///////////////////////

	//
	public $services = array(
		"1" => "Classic",
		"2" => "Private",
		"3" => "pickup",
		"4" => "express 10:00",
		"5" => "express 12:00",
		"6" => "express 18:00",
		"7" => "Private večerní doručení",
		"8" => "Private sobotní doručení",
	);
	//přenest do validatoru
	public $required_properties = array(
		'service_type', 'rec_name', 'rec_email', 'rec_street', 'rec_city',
		'rec_zip', 'rec_country', 'eid',
	);
}