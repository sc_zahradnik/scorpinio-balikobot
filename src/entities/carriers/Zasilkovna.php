<?php
namespace Scorpinio\Balikobot\entities\carriers;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\entities\Carrier;

class Zasilkovna extends Carrier{
	///////////////
	// CONSTANTS //
	///////////////
	const ID = "zasilkova";
	const NAME = "Zásilkovna";
	const TRACK_URL = "https://www.zasilkovna.cz/vyhledavani?number={package_id}&name={name}&surname={lastname}";

	///////////////////////
	// PUBLIC PROPERTIES //
	///////////////////////

	//
	public $services = array(
	);
	//přenest do validatoru
	public $required_properties = array(
		'branch_id','rec_name', 'rec_email', 'rec_street', 'rec_city',
		'rec_zip', 'rec_country', 'price', 'eid',
	);
}