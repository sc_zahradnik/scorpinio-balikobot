<?php
namespace Scorpinio\Balikobot\entities\carriers;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\entities\Carrier;

class Ulozenka extends Carrier{
	///////////////
	// CONSTANTS //
	///////////////
	const ID = "ulozenka";
	const NAME = "Uloženka";
	const TRACK_URL = "https://tracking.ulozenka.cz/{package_id}";

	///////////////////////
	// PUBLIC PROPERTIES //
	///////////////////////

	//
	public $services = array(
		1 => "Uloženka",
		2 => "Zmluvný balík s poistením – Slovenská pošta",
		3 => "DPD Classic",
		4 => "DPD Private",
		5 => "DPD ParceShop",
		6 => "Balík do ruky - Česká pošta",
		7 => "Balík na poštu - Česká pošta",
		8 => "InPost výdejní automaty",
		9 => "InTime Balíkomat",
		10 => "Partner PNS",
		11 => "Partner",
	);
	//přenest do validatoru
	public $required_properties = array(
	);
}