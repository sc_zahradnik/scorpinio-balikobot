<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Package extends Entity{
	/* REQUEST DATA */
	protected $eid;
	protected $order_number;
	protected $real_order_id;
	protected $service_type;
	protected $branch_id;
	protected $price;
	protected $del_insurance;
	protected $del_evening;
	protected $del_exworks;
	protected $cod_price;
	protected $cod_currency;
	protected $vs;
	protected $rec_name;
	protected $rec_firm;
	protected $rec_street;
	protected $rec_city;
	protected $rec_zip;
	protected $rec_region;
	protected $rec_country;
	protected $rec_email;
	protected $rec_phone;
	protected $weight;
	protected $require_full_age;
	protected $password;
	protected $credit_card;
	protected $sms_notification;
	protected $services;
	protected $width;
	protected $length;
	protected $height;
	protected $note;
	protected $swap;
	protected $vdl_service;
	protected $volume;
	protected $pieces_count;

	/* RESPONSE DATA */
	protected $id_package;
	protected $id_carrier;
	protected $label_url;

}