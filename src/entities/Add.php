<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Add extends Entity{
	///////////////
	// CONSTANTS //
	///////////////
	const METHOD = Request::METHOD_POST;

	//////////////////////////
	// PROTECTED PROPERTIES //
	//////////////////////////

	protected $url;
	protected $carrier;
	protected $packages;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * @param Carrier        $carrier  instance of carrier
	 * @param array|Package  $packages 
	 */
	public function __construct(Carrier $carrier, $packages){
		if ($packages instanceof Package) {
			$packages = array($packages);
		}elseif(is_array($packages)){
			foreach ($packages as $index => $package) {
				if (!($package instanceof Package)) {
					//throw
				}
			}
		}else{
			throw new \Exception("Scorpinio\\Balikobot\\entities\\Add::_construct - $packages must by instanceof Scorpinio\Balikobot\entities\Package or array of it.");
		}

		$url = $carrier::ID."/add";

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
			'packages' => $packages,
		));
	}
	/**
	 * Prepare request data
	 * @return array
	 */
	public function getData(){
		$data = array();
		foreach ($this->packages as $key => $package) {
			$data[] = $package->getData();
		}
		return $data;
	}

}