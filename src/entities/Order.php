<?php
namespace Scorpinio\Balikobot\entities;

use Scorpinio\Balikobot\abstracts\Entity;
use Scorpinio\Balikobot\Request;

class Order extends Entity{
	///////////////
	// CONSTANTS //
	///////////////
	const METHOD = Request::METHOD_POST;

	//////////////////////////
	// PROTECTED PROPERTIES //
	//////////////////////////

	protected $url;
	protected $carrier;
	protected $packages;
	protected $note;

	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	/**
	 * [__construct description]
	 * @param Carrier $carrier  [description]
	 * @param [type]  $packages [description]
	 * @param string  $note     [description]
	 */
	public function __construct(Carrier $carrier, $packages, $note=""){
		$url = $carrier::ID."/order";

		parent::__construct(array(
			'url' => $url,
			'carrier' => $carrier,
			'packages' => $packages,
			'note' => $note,
		));
	}
	/**
	 * [getData description]
	 * @return [type] [description]
	 */
	public function getData(){
		$data = array(
			'package_ids' => $this->packages,
		);
		if ($this->carrier instanceof carriers\PPL) {
			$data['date'] = date("Y-m-d", time());
			$data['note'] = $this->note;
		}	 
		return $data;
	}
}