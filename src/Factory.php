<?php
namespace Scorpinio\Balikobot;

class Factory{
	/**
	 * @param  string $id  carrier id
	 * @return Entity|null instance of carrier
	 */
	public static function createCarrierById($id){
		$id = strtolower($id);
		$serviceType = "";
		if (strpos($id, "|") !== false) {
			$tmp = explode("|", $id);
			$id = $tmp[0];
			$serviceType = $tmp[1];
		}

		switch ($id) {
			case 'cp':
				return new entities\carriers\CP($serviceType);
			break;
			case 'dpd':
				return new entities\carriers\DPD($serviceType);
			break;
			case 'geis':
				return new entities\carriers\Geis($serviceType);
			break;
			case 'ppl':
				return new entities\carriers\PPL($serviceType);
			break;
			case 'ulozenka':
				return new entities\carriers\Ulozenka($serviceType);
			break;
			case 'zasilkovna':
				return new entities\carriers\Zasilkovna($serviceType);
			break;
			default:
				return;
			break;
		}
	}

	/**
	 * @return array list of available carriers
	 */
	public static function getCarriers(){
		return array(
			'cp'         => new entities\carriers\CP(),
			'dpd'        => new entities\carriers\DPD(),
			'geis'       => new entities\carriers\Geis(),
			'ppl'        => new entities\carriers\PPL(),
			'ulozenka'   => new entities\carriers\Ulozenka(),
			'zasilkovna' => new entities\carriers\Zasilkovna(),
		);
	}
}