<?php
// Balikobot user data
$user = "";
$pswd = "";

$api = new \Scorpinio\Balikobot\Api($user, $pswd);

// example for carrier Česká pošta
// create carrier by carrier id 
// cp = carrier Česká pošta
// cp|DR = carrier Česká pošta with selected service type "Balík do ruky"
$carrier = \Scorpinio\Balikobot\Factory::createCarrierById("cp|DR");

// create package for add to balikobot
$package = new \Scorpinio\Balikobot\entities\Package(array(
	'eid'              => 1,
	'service_type'     => "DR",
	'price'            => 100.00,
	'rec_name'         => "Testínka Rohlíková",
	'rec_firm'         => "",
	'rec_street'       => "Dalamánkova 4",
	'rec_city'         => "Pečivov",
	'rec_zip'          => 60200,
	'rec_country'      => "CZ",
	'rec_email'        => "zahradnik@scorpinio.net",
	'rec_phone'        => "+420123456789",
	'weight'           => 5,
));

// create request
$add = new \Scorpinio\Balikobot\entities\Add($carrier, $package);

// do request
$response = $api->request($add);

var_dump($response);
