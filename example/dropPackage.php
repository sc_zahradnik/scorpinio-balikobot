<?php
// Balikobot user data
$user = "";
$pswd = "";

$api = new \Scorpinio\Balikobot\Api($user, $pswd);

// example for carrier Česká pošta
// create carrier by carrier id 
// cp = carrier Česká pošta
// cp|DR = carrier Česká pošta with selected service type "Balík do ruky"
$carrier = \Scorpinio\Balikobot\Factory::createCarrierById("cp|DR");

// create request
$drop = new \Scorpinio\Balikobot\entities\Drop($carrier, array(12345, 23456));

// do request
$response = $api->request($drop);

var_dump($response);
